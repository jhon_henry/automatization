package userinterface;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class SearchCoursePage extends PageObject {
    public static final Target BUTTON_UC = Target.the("Selected Choucair")
            .located(By.xpath("//div[@class='card dashboard-card']//a"));
    public static final Target INPUT_COURSE = Target.the("Search the course")
            .located(By.id("coursesearchbox"));
    public static final Target BUTTON_GO = Target.the("button for selected course")
            .located(By.xpath("//button[@class='btn btn-secondary']"));
    public static final Target SELECTED_COURSE = Target.the("button for selected course")
            .located(By.xpath("//h4[contains(text(),'Recursos Automatización Bancolombia')]"));
    public static final Target NAME_COURSE     = Target.the("Find name course's")
            .located(By.xpath("//h1[contains(text(),'Analista Bancolombia')]"));
}
